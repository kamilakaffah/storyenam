from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import Message
from .forms import MessageForm
from datetime import datetime

# Create your views here.

def home(request):
    data = Message.objects.all().values()
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            status = Message()
            status.pesan = form.cleaned_data['your_message']
            status.date = datetime.now()
            status.save()
            return HttpResponseRedirect('/')
    else:
        form = MessageForm()

    response = {
        'form' : form,
        'data' : data,
    }
    return render(request, 'index.html', response)
