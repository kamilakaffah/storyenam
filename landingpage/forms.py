from django import forms
from .models import Message


class MessageForm(forms.Form):
    your_message = forms.CharField(label = "Status", max_length = 300, widget = forms.TextInput(
        attrs = {'required':True,}
    ))
