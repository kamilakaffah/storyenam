from django.http import HttpRequest
from django.urls import resolve
from django.utils import timezone
from . import views
import time 
import unittest
from django.test import TestCase, Client, LiveServerTestCase
from .views import *
from .models import *
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class SampleTest(TestCase):
    def test_homepage(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.home)

    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_form(self):
        response = Client().get('/')
        self.assertIn('Status', response.content.decode())

    def test_create_new_model(self):
        new_message = Message.objects.create(waktu=timezone.now(), pesan="Alhamdulillah")
        count_pesan = Message.objects.all().count()

        self.assertEqual(count_pesan, 1)

    def test_form_validated(self):
        form = MessageForm(data={'pesan':'', 'waktu':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['your_message'],
            ['This field is required.']
        )

    def test_message_completed(self):
        test_str = 'Saya senang'
        response_post = Client().post('/', {'pesan':test_str,'waktu':timezone.now})
        self.assertEqual(response_post.status_code,200)
        response = Client().get('/')
        html_response = response.content.decode('UTF-8')


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_op = Options()
        chrome_op.add_argument('--dns-prefetch-disable'),
        chrome_op.add_argument('--no-sandbox'),
        chrome_op.add_argument('--headless'),
        chrome_op.add_argument('disable-gpu'),
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_op)
        super(FunctionalTest, self).setUp()
    
    def test_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        new_status = selenium.find_element_by_id('id_your_message')
        new_status.send_keys("Coba coba")
        time.sleep(3)
        submit = selenium.find_element_by_id('tombol')
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        selenium.get(self.live_server_url)
        time.sleep(3)
        self.assertIn('Coba coba', selenium.page_source)
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()